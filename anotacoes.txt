https://docs.docker.com/machine/install-machine/

base=https://github.com/docker/machine/releases/download/v0.16.0 &&
  curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
  sudo mv /tmp/docker-machine /usr/local/bin/docker-machine &&
  chmod +x /usr/local/bin/docker-machine

https://www.virtualbox.org/wiki/Linux_Downloads

`docker-machine ls` - lista todas docker machines criadas

`docker-machine create -d virtualbox NOME_VM` - cria docker machine com base no virtualbox(precisa do virtual box instalado) e nome dado.

`docker-machine start vm1` - para iniciar vm

`docker-machine ssh vm1` - se conecta na máquina virtual

`docker-machine rm -f NOME_VM` - deleta máquina virtual pelo nome

Uso do docker-machine para criar servidores na AWS
https://docs.docker.com/machine/examples/aws/

`docker swarm init --advertise-addr IP_MAQUINA_FISICA` - inicializar Swarm e uma boa prática adicionar o ip da máquina física

`docker swarm join --token SWMTKN-1-3y6vpk3c5v5sm5of3cfaaetrn3l80lf5wsu2xwqq2en32gtdj7-05xw8nicdfzuhvhpje91nskri 192.168.99.100:2377` - adicionar worker ao Swarm

`docker swarm join-token worker` - recuperar token para adicionar worker

`docker node ls`- lista cada ID e hostname que faz parte do Swarm. Worker não podem visualizar ou modificar o estado do cluster.

`docker swarm leave` - remove worker do Swarm

`docker node rm ID` - remove worker do Manager

** Rodar somente no nó manager**

`docker node inspect vm2` - listar informações da vm2. Deve ser rodado de um nó manager.

`docker service create -p 8080:3000 aluracursos/barbearia` - cria serviço/containers no escopo do Swarm

`docker service ls` - verificar serviços no Swarm

`docker service ps ID` - verificar detalhes do serviço no Swarm

***backup***
/var/lib/docker/swarm - pasta com infomação de toda configuração do Swarm

copiar para /home/root da vm manager

recopiar para copiar para /home/root da vm manager

`docker swarm init --force-new-cluster --advertise-addr 192.168.99.100` - força a recriar cluster usando backup

** vários managers
`docker swarm join-token manager` - recupara token para criar outro manager

`docker swarm join --token MANAGER_TOKEN 192.168.99.100:2377` - adicionar **manager** ao Swarm

`docker node ls --format "{{.Hostname}} {{.ManagerStatus}}"` - formatar visulizar somente alguns atributos

****Raft****
Suporta = (N-1)/ 2  - falhas

Mínimo: (N/2) + 1 - de quórum

N = número de managers

Mais managers menor desempenho

Recomenda-se 3,5 ou 7 managers. Mas nunca mais que 10.

** Readicionar um manager **
`docker node demote vm1` - rebaixar vm1 para worker

`docker node rm ID` - remove worker do Manager

`docker swarm join-token manager` - recuperar token para adicionar manager

`docker swarm join --token SWMTKN-1-2noo8v4tioehk7tjo7orgsb1mg4xq4xxo3ech2m32628flh4p7-0fj9spvcwlih089hrcymw1sg7 192.168.99.101:2377 --advertise-addr 192.168.99.100` - advertise-add para manter ip original

** Bloquear serviços na Manager **
`docker service rm $(docker service ls -q)` - remove todos os serviços ativos

`docker node update --availability drain vm2`- restringimos o uso de serviço na vm2. NENHUM serviço será rodado.

** Restringir os serviços ** 
`docker service update --constraint-add node.role==worker ID` - serviço ID rodará somente em workers

`docker service update --constraint-add node.id==t76gee19fjs8 ci10k3u7q6ti - restrição por ID

`docker service update --constraint-add node.hostname==vm4 ci10k3u7q6ti` - retrição por hostname

`docker service update --constraint-rm node.hostname==vm4 ci10k3u7q6ti` - remove restrição por ID

`docker service update --constraint-rm node.hostname==vm4 ci10k3u7q6ti` - remove restrição por hostname

** Replicas ***
`docker service update --replicas 5 ci10k3u7q6ti` - modificar replicas do ID

`docker service scale ID=5` - modificar replicas do ID pelo scale.

*** Globais ****
`docker service create -p 8080:3000 --mode global aluracursos/barbearia`

*** Rede ****
`docker service create --name servico --network my_overlay --replicas 2 alpine sleep 1d`

**Comunicação entre serviços e containers "standalone" **
`docker network create -d overlay --attachable my_overlay` - Comunicação entre serviços e containers "standalone"

**Stack**
`docker stack deploy --compose-file docker-compose.yml vote` - criar stack
`docker stack ls` - lista as stacks
`docker stack rm vote` - deleta stack

